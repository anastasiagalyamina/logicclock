import java.util.*;

public class Process {
	
	int innerTime = 0;
	int frequency;
	Timer timer = new Timer();
    timer.schedule(event, frequency);
    
    //   по таймеру происходит событие
    TimerTask event = new TimerTask() {
    	@Override  
        public void run() {
            innerTime++;
        }
    }; 

    //     в процесс передается частота наступления событий
	public Process(int frequency) {
		this.frequency = frequency;
	}
	
	public int GetTime() {
		return innerTime;
	}		
	
	//  при получении сообщения процесс выбирает наибольшее значение из своего и полученного времени         
	public void GetMessage(int Time) {
		innerTime = innerTime >= Time ? innerTime : Time;
	}
}

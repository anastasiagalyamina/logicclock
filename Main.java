import java.util.ArrayList;
import java.util.Random;

public class Main {
	//   Метод временных меток Лампорта
	static int time;
	static int size = 20;
	private static ArrayList<Process> processes = new ArrayList<>();
	static Random rnd = new Random();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CreateProcesses();
		SendMessages();
		PrintTime();

	}
	
	private static void SendMessages() {
		for (int i = 0; i < 100; i ++) {
			// случайно выбираем 2 процесса: один отправляет сообщение, другой принимает
			int process1 = rnd.nextInt(size);
			int process2 = rnd.nextInt(size);	
			// один процесс отправляет сообщение второму и передает ему своё время   
			time = processes.get(process1).GetTime();
			processes.get(process2).GetMessage(time);
			
		}
	}
	
	private static void CreateProcesses() {
		for(int i = 0; i < size; i++) {
			// в каджом процессе србытия происходят раз в 1-10 секунд
			int time = rnd.nextInt(10);			
			processes.add(new Process(time));
		}
	}
	
	private static void PrintTime() {
		for(int i = 0; i < size; i ++) {
			System.out.println("Время процесса  = " + processes.get(i).GetTime());
		}
	}

}
